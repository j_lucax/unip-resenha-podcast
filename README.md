Como trabalho complementar a nota do segundo bimestre, cada aluno deverá fazer
um fork do projeto https://gitlab.com/prof.brunocesar/unip-resenha-podcast e nele
fazer sua resenha no arquivo Word existente no projeto. Após isso, o aluno deverá 
criar um Marge Request do arquivo word da Resenha do projeto na branch master
do meu projeto (https://gitlab.com/prof.brunocesar/unip-resenha-podcast) de forma 
que eu possa corrigi-lo. 

Seguem alguns links como SUGESTÃO para entendimento do processo de Marge Request:
*  https://gitlab.com/ad-si-2015-1/projeto1/wikis/tutorial-como-submeter-uma-merge-request
*  https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
*  https://docs.gitlab.com/ee/user/project/merge_requests/
*  https://www.tutorialspoint.com/gitlab/gitlab_merge_requests.htm

A resenha deve possuir no mínimo 15 e no máximo 30 linhas. Nem o tamanho nem a fonte já
definidas no documento disponibilizado como modelo não devem ser alteradas sob pena de
aplicação de 70% de redutor na nota alcançada a título de penalidade pelo não atendimento
deste requisito.

O prazo limite para conclusão da resenha e compartilhamento do arquivo para correção é dia 
17 de novembro de 2019 até no máximo 23h59.

No arquivo Word, o aluno deverá informar seu nome, turma e RA no local indicado.

Resenha do PodCast NerdTech 19 - Profissão programador 3.0
*  URL: https://jovemnerd.com.br/nerdcast/nerdtech/profissao-programador-3-0/
*  Mínimo de 15 linhas, máximo de 30 (linha de separação dos parágrafos não será contada)
*  Tópicos a serem abordados (sob a perspectiva do PodCast):
   *  Designers (FrontEnd) vs Programadores (BackEnd)
   *  Por onde (tecnologia) começar os estudos? Por que? Como estudar?
   *  Discorra sobre a(s) principal(is) característica(s) para ser um bom programador

Total de pontos do trabalho na nota do segundo bimestre: 3 pontos
Distribuição dos pontos:
*  Solicitação de Marge Request do arquivo word da resenha do podcast - 0.6 ponto
*  Resenha do PodCast - 0.8 ponto para cada tópico abordado (2.4 pontos no máximo)

No caso de eu encontrar resenhas idênticas (ou mesmo muito similares, segundo a minha 
avaliação), todas as ocorrências receberão a nota ZERO no trabalho, inclusive a original
que serviu de modelo para as demais.


